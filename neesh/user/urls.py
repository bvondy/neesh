from django.urls import path
from .views import home, RegisterView

urlpatterns = [
    path("", home, name="user-home"),
    path("register/", RegisterView.as_view(), name="users-register"),
]
